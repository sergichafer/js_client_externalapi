var endpoint = {};
endpoint.home = [
    {
        "pk": 1,
        "titulo": "Wifibytes",
        "subtitulo": "Otra forma de hacer comunicación",
        "caja_izquierda_titulo": "¿Por qué Wifibytes?",
        "caja_izquierda_texto": "<p dir=\"ltr\"><span>&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos asistidos.</span></p>\r\n<p>&nbsp;</p>\r\n<p dir=\"ltr\"><span>Simple y facil. Todo el soporte y atenci&oacute;n v&iacute;a internet con un sistema de tickets.</span></p>\r\n<p>&nbsp;</p>",
        "caja_derecha_titulo": "¿Por qué Wifibytes?",
        "caja_derecha_texto": "<p><span>&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos </span></p>\r\n<p>&nbsp;</p>",
        "activo": true,
        "idioma": 1,
        "lang": "es"
    },
    {
        "pk": 2,
        "titulo": "Wifibytes",
        "subtitulo": "Altra forma de fer comunicació.",
        "caja_izquierda_titulo": "Per què Wifibytes?",
        "caja_izquierda_texto": "<p>Vols donar-te de baixa? Canviar de tarifa? Activar el roaming? &iquest;Desactivar la b&uacute;stia de oz? Sense problema. Entra en el teu compte i fes-ho amb un simple click.Sense preguntes, sense trucades a tel&egrave;fons assistits.</p>\r\n<p>Simple i f&agrave;cil. Tot el suport i atenci&oacute; via internet amb un sistema de tiquets.</p>\r\n<p>&nbsp;</p>",
        "caja_derecha_titulo": "Per què Wifibytes?",
        "caja_derecha_texto": "<p>Vols donar-te de baixa? Canviar de tarifa? Activar el roaming? &iquest;Desactivar la b&uacute;stia de oz? Sense problema. Entra en el teu compte i fes-ho amb un simple click.Sense preguntes, sense trucades a tel&egrave;fons assistits.</p>\r\n<p>Simple i f&agrave;cil. Tot el suport i atenci&oacute; via internet amb un sistema de tiquets.</p>\r\n<p>&nbsp;</p>",
        "activo": true,
        "idioma": 3,
        "lang": "val"
    },
    {
        "pk": 3,
        "titulo": "Wifibytes",
        "subtitulo": "A different approach to modern communication",
        "caja_izquierda_titulo": "Why wifibytes?",
        "caja_izquierda_texto": "<p>Do you want to unsubscribe? Change the rate? Activate roaming? Deactivate the voicemail? No problem. Enter your account and do it with a single click. Without questions or calls to assisted phones.<br /><br />Simple and easy. Support and attention via internet with a ticket system.</p>",
        "caja_derecha_titulo": "Why wifibytes?",
        "caja_derecha_texto": "<p>Do you want to unsubscribe? Change the rate? Activate roaming? Deactivate the voicemail? No problem. Enter your account and do it with a single click. Without questions or calls to assisted phones.<br /><br />Simple and easy. Support and attention via internet with a ticket system.</p>",
        "activo": true,
        "idioma": 2,
        "lang": "en"
    }
];
endpoint.tarifa = {
    
    "count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "codtarifa": 1,
            "nombretarifa": "Egg",
            "slug": "egg",
            "pretitulo": "Huevo",
            "logo": "http://127.0.0.1:8000/media/Logo/icon-egg_FRpw391.png",
            "precio": 5.98,
            "activo": true,
            "destacado": true,
            "color": {
                "id": 1,
                "titulo": "green",
                "hexadecimal": "#99ff99"
            },
            "subtarifas": [
                {
                    "subtarifa_id": 2,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": 10.0,
                    "subtarifa_velocidad_conexion_bajada": 5.0,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "",
                    "subtarifa_omv": {
                        "codigo": 2,
                        "nombre": "Movistar",
                        "descripcion": "Lorem Ipsum",
                        "activo": true
                    },
                    "tipo_tarifa": 1,
                    "subtarifa_tarifa": {
                        "codtarifa": 1,
                        "nombretarifa": "Egg",
                        "slug": "egg",
                        "pretitulo": "Huevo",
                        "pretitulo_va": "Ou",
                        "logo": "media/Logo/icon-egg_FRpw391.png",
                        "precio": 5.98,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540985884,
                        "updated_at": 1540986485,
                        "color": 1
                    }
                }
            ]
        },
        {
            "codtarifa": 2,
            "nombretarifa": "Chicken",
            "slug": "chicken",
            "pretitulo": "Pollo",
            "logo": "http://127.0.0.1:8000/media/Logo/icon-chicken.png",
            "precio": 15.95,
            "activo": true,
            "destacado": true,
            "color": {
                "id": 1,
                "titulo": "green",
                "hexadecimal": "#99ff99"
            },
            "subtarifas": [
                {
                    "subtarifa_id": 1,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": 30.0,
                    "subtarifa_velocidad_conexion_bajada": 15.0,
                    "subtarifa_num_canales": 34,
                    "subtarifa_siglas_omv": "Paquete Fútbol",
                    "subtarifa_omv": {
                        "codigo": 2,
                        "nombre": "Movistar",
                        "descripcion": "Lorem Ipsum",
                        "activo": true
                    },
                    "tipo_tarifa": 1,
                    "subtarifa_tarifa": {
                        "codtarifa": 2,
                        "nombretarifa": "Chicken",
                        "slug": "chicken",
                        "pretitulo": "Pollo",
                        "pretitulo_va": "Pollastre",
                        "logo": "media/Logo/icon-chicken.png",
                        "precio": 15.95,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540986047,
                        "updated_at": 1540986422,
                        "color": 1
                    }
                }
            ]
        },
        {
            "codtarifa": 3,
            "nombretarifa": "Chick",
            "slug": "chick",
            "pretitulo": "Pollito",
            "logo": "http://127.0.0.1:8000/media/Logo/chick_8qOw42B.png",
            "precio": 15.0,
            "activo": true,
            "destacado": true,
            "color": {
                "id": 1,
                "titulo": "green",
                "hexadecimal": "#99ff99"
            },
            "subtarifas": [
                {
                    "subtarifa_id": 3,
                    "subtarifa_datos_internet": 12.0,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": true,
                    "subtarifa_velocidad_conexion_subida": 6.0,
                    "subtarifa_velocidad_conexion_bajada": 4.0,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "",
                    "subtarifa_omv": null,
                    "tipo_tarifa": 1,
                    "subtarifa_tarifa": {
                        "codtarifa": 3,
                        "nombretarifa": "Chick",
                        "slug": "chick",
                        "pretitulo": "Pollito",
                        "pretitulo_va": "Pollet",
                        "logo": "media/Logo/chick_8qOw42B.png",
                        "precio": 15.0,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1540992002,
                        "updated_at": 1542130915,
                        "color": 1
                    }
                }
            ]
        },
        {
            "codtarifa": 4,
            "nombretarifa": "Chicken2",
            "slug": "chicken2",
            "pretitulo": "Pollo2",
            "logo": "http://127.0.0.1:8000/media/Logo/icon-chicken_cA7B0et.png",
            "precio": 26.0,
            "activo": true,
            "destacado": true,
            "color": {
                "id": 1,
                "titulo": "green",
                "hexadecimal": "#99ff99"
            },
            "subtarifas": [
                {
                    "subtarifa_id": 4,
                    "subtarifa_datos_internet": null,
                    "subtarifa_cent_minuto": null,
                    "subtarifa_est_llamada": null,
                    "subtarifa_precio_sms": null,
                    "subtarifa_minutos_gratis": null,
                    "subtarifa_minutos_ilimitados": false,
                    "subtarifa_velocidad_conexion_subida": 50.0,
                    "subtarifa_velocidad_conexion_bajada": 100.0,
                    "subtarifa_num_canales": null,
                    "subtarifa_siglas_omv": "",
                    "subtarifa_omv": {
                        "codigo": 2,
                        "nombre": "Movistar",
                        "descripcion": "Lorem Ipsum",
                        "activo": true
                    },
                    "tipo_tarifa": 1,
                    "subtarifa_tarifa": {
                        "codtarifa": 4,
                        "nombretarifa": "Chicken2",
                        "slug": "chicken2",
                        "pretitulo": "Pollo2",
                        "pretitulo_va": "Pollastre2",
                        "logo": "media/Logo/icon-chicken_cA7B0et.png",
                        "precio": 26.0,
                        "activo": true,
                        "destacado": true,
                        "created_at": 1542226014,
                        "updated_at": 1542283805,
                        "color": 1
                    }
                }
            ]
        }
    ]
};
endpoint.datos_empresa = {
    "name": "Phoned Co.",
    "cifnif": "135555522552554366",
    "phone": "123456789",
    "logo": "http://127.0.0.1:8000/media/logo/3ad95f57-18f8-4406-ab37-13b472a8598a.png",
    "icon_logo": "http://127.0.0.1:8000/media/icon_logo/3ad95f57-18f8-4406-ab37-13b472a8598a.png",
    "mapa_cobertura": null,
    "address": "Rizos & Mechas Peluqueros, Carrer d'Àngel Guimerà, 75",
    "city": "Valencia",
    "province": "Valencia",
    "country": "Espanya",
    "zipcode": "46011",
    "location_lat": "39.469049",
    "location_long": "-0.393247",
    "facebook": "https://twitter.com/wifibytes",
    "twitter": "https://www.facebook.com/wifibytes",
    "textos": [
        {
            "key": "jumbotron_3",
            "content": "<p>Lorem Ipsum</p>",
            "image": "http://localhost:8000/media/info_empresa_image/action-3382792_1920.jpg",
            "lang": "es"
        },
        {
            "key": "jumbotron_4",
            "content": "<p>Lorem Ipsum</p>",
            "image": "http://localhost:8000/media/info_empresa_image/macbook-336704_1920.jpg",
            "lang": "es"
        },
        {
            "key": "jumbotron_1",
            "content": "<p>Lorem Ipsum</p>",
            "image": "http://localhost:8000/media/info_empresa_image/girl-1245713_1920.jpg",
            "lang": "es"
        },
        {
            "key": "jumbotron_2",
            "content": "<p>Lorem Ipsum</p>",
            "image": "http://localhost:8000/media/info_empresa_image/mobile-phone-1875813_1920.jpg",
            "lang": "es"
        },
        {
            "key": "legal_advise",
            "content": "<div class=\"ng-binding\">\r\n<p dir=\"ltr\"><strong><span>DATOS GENERALES</span></strong></p>\r\n<p dir=\"ltr\"><span>De acuerdo con el art&iacute;culo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico ponemos a su disposici&oacute;n los siguientes datos: </span></p>\r\n<p dir=\"ltr\"><span>Wifibytes SL. est&aacute; domiciliada en la calle C/Batalla de Lepanto n&ordm;5, Bajo (Bocairent), con CIF B98137078.Inscrita en el Registro Mercantil de Valencia, Vol .9030 , Folio 34, Hoja V-.133779, Inscripci&oacute;n 4&ordf;.</span></p>\r\n<p dir=\"ltr\"><span>En la web </span><a href=\"http://wifibytes-front.wearecactus.com/#/\"><span>http://wifibytes-front.wearecactus.com/#/</span></a><span>hay una serie de contenidos de car&aacute;cter informativo sobre </span><a href=\"http://wifibytes-front.wearecactus.com/#/nosotros\"><span>http://wifibytes-front.wearecactus.com/#/nosotros</span></a></p>\r\n<p dir=\"ltr\"><span>Su principal objetivo</span><span> es facilitar a los clientes y al p&uacute;blico en general, la informaci&oacute;n relativa a la empresa, a los productos y servicios que se ofrecen </span><span>.</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>POL&Iacute;TICA DE PRIVACIDAD</span></strong></p>\r\n<p dir=\"ltr\"><span>En cumplimiento de lo dispuesto en </span><span>la Ley Org&aacute;nica 15/1999, de 13 de Diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal (LOPD) </span><span>se informa al usuario que todos los datos que nos proporcione ser&aacute;n incorporados a un fichero, creado y mantenido bajo la responsabilidad de Wifibytes SL</span></p>\r\n<p dir=\"ltr\"><span>Siempre</span><span> se va a respetar la confidencialidad de sus datos personales </span><span>que s&oacute;lo ser&aacute;n utilizados con la finalidad de gestionar los servicios ofrecidos, atender a las solicitudes que nos plantee, realizar tareas administrativas, as&iacute; como remitir informaci&oacute;n t&eacute;cnica, comercial o publicitaria por v&iacute;a ordinaria o electr&oacute;nica.</span></p>\r\n<p dir=\"ltr\"><span>Para ejercer sus </span><span>derechos de oposici&oacute;n, rectificaci&oacute;n o cancelaci&oacute;n </span><span>deber&aacute; dirigirse a la sede de la empresa C/Batalla de Lepanto, n&ordm;5 Bajo (Bocairent), escribirnos al siguiente correo info@wifibytes.com o ll&aacute;manos al 960 500 606</span></p>\r\n<p dir=\"ltr\"><strong><span>CONDICIONES DE USO</span></strong></p>\r\n<p dir=\"ltr\"><span>Las condiciones de acceso y uso del presente sitio web se rigen por la legalidad vigente y por el principio de buena fe</span><span> comprometi&eacute;ndose el usuario a realizar un buen uso de la web. No se permiten conductas que vayan contra la ley, los derechos o intereses de terceros.</span></p>\r\n<p dir=\"ltr\"><span>Ser usuario de la web de </span><a href=\"http://web.wifibytes.com/front/#/\"><span>http://wifibytes.wearecactus.com/front/#/</span></a><span>implica que reconoce haber le&iacute;do y aceptado las presentes condiciones y lo que las extienda la normativa legal aplicable en esta materia. Si por el motivo que fuere no est&aacute; de acuerdo con estas condiciones no contin&uacute;e usando esta web.</span></p>\r\n<p dir=\"ltr\"><span>Cualquier tipo de notificaci&oacute;n y/o reclamaci&oacute;n solamente ser&aacute; v&aacute;lida por notificaci&oacute;n escrita y/o correo certificado.</span></p>\r\n<p dir=\"ltr\"><span><strong>RESPONSABILIDADES</strong> </span></p>\r\n<p dir=\"ltr\"><span>Wifibytes SL no se hace responsable de la informaci&oacute;n y contenidos almacenados en foros, redes sociales o cualesquier otro medio</span><span> que permita a terceros publicar contenidos de forma independiente en la p&aacute;gina web del prestador.</span></p>\r\n<p dir=\"ltr\"><span>Sin embargo, teniendo en cuenta los art. 11 y 16 de la LSSI-CE,Wifibytes SL </span><span>se compromete a la retirada o en su caso bloqueo de aquellos contenidos que pudieran afectar o contravenir la legislaci&oacute;n nacional o internacional</span><span>, derechos de terceros o la moral y el orden p&uacute;blico.</span></p>\r\n<p dir=\"ltr\"><span>Tampoco la empresa se responsabilizar&aacute; de los da&ntilde;os y perjuicios que se produzcan por fallos o malas configuraciones del software instalado en el ordenador del internauta.</span><span> Se excluye toda responsabilidad por alguna incidencia t&eacute;cnica o fallo que se produzca cuando el usuario se conecte a internet. Igualmente no se garantiza la inexistencia de interrupciones o errores en el acceso al sitio web.</span></p>\r\n<p dir=\"ltr\"><span>As&iacute; mismo Wifibytes SL </span><span>se reserva el derecho a actualizar, modificar o eliminar la informaci&oacute;n contenida en su p&aacute;gina web</span><span>, as&iacute; como la configuraci&oacute;n o presentaci&oacute;n del mismo, en cualquier momento sin asumir alguna responsabilidad por ello.</span></p>\r\n<p dir=\"ltr\"><span>Le comunicamos que cualquier precio que pueda ver en nuestra web ser&aacute; solamente orientativo. Si el usuario desea saber con exactitud el precio o si el producto en el momento actual cuenta con alguna oferta de la cual se puede beneficiar ha de acudir a alguna de las tiendas f&iacute;sicas con las que cuenta Wifibytes SL.</span></p>\r\n<p dir=\"ltr\"><strong><span>PROPIEDAD INTELECTUAL E INDUSTRIAL</span></strong></p>\r\n<p dir=\"ltr\"><span>Wifibytes SL.</span><span> es titular de todos los derechos sobre el software de la publicaci&oacute;n digital as&iacute; como de los derechos de propiedad industrial e intelectual referidos a los contenidos que se incluyan</span><span>, a excepci&oacute;n de los derechos sobre productos y servicios de car&aacute;cter p&uacute;blico que no son propiedad de esta empresa.</span></p>\r\n<p dir=\"ltr\"><span>Ning&uacute;n material publicado en esta web podr&aacute; ser reproducido, copiado o publicado</span><span> sin el consentimiento por escrito de Wifibytes SL.</span></p>\r\n<p dir=\"ltr\"><span>Toda la informaci&oacute;n que se reciba en la web, como comentarios, sugerencias o ideas, se considerar&aacute; cedida</span><span> a Wifibytes SL.de manera gratuita. No debe enviarse informaci&oacute;n que NO pueda ser tratada de este modo.</span></p>\r\n<p dir=\"ltr\"><span>Todos los productos y servicios de estas p&aacute;ginas que NO son propiedad de Wifibytes SL. son marcas registradas de sus respectivos propietarios y son reconocidas como tales por nuestra empresa. Solamente aparecen en la web de Wifibytes SL. a efectos de promoci&oacute;n y de recopilaci&oacute;n de informaci&oacute;n. Estos propietarios pueden solicitar la modificaci&oacute;n o eliminaci&oacute;n de la informaci&oacute;n que les pertenece.</span></p>\r\n<p dir=\"ltr\"><strong><span>LEY APLICABLE Y JURISDICCI&Oacute;N</span></strong></p>\r\n<p dir=\"ltr\"><span>Las presentes condiciones generales se rigen por la legislaci&oacute;n espa&ntilde;ola.</span><span> Para cualquier litigio que pudiera surgir relacionado con el sitio web o la actividad que en &eacute;l se desarrolla ser&aacute;n competentes Juzgados de Ontinyent, renunciando expresamente el usuario a cualquier otro fuero que pudiera corresponderle.</span></p>\r\n</div>",
            "image": null,
            "lang": "es"
        },
        {
            "key": "cookies",
            "content": "<div class=\"ng-binding\">\r\n<p dir=\"ltr\"><strong><span>Pol&iacute;tica de Cookies en Wifibytes</span></strong></p>\r\n<p dir=\"ltr\"><span>Nuestro sitio web </span><a href=\"http://wifibytes-front.wearecactus.com/#/\"><span>http://wifibytes-front.wearecactus.com/#/</span></a><span> utiliza una tecnolog&iacute;a denominada &ldquo;cookies&rdquo;, con la finalidad de poder recabar informaci&oacute;n acerca del uso del sitio web.</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><span>Le informamos que vamos a utilizar cookies con la finalidad de facilitar su navegaci&oacute;n a trav&eacute;s del sitio Web, distinguirle de otros usuarios, proporcionarle una mejor experiencia en el uso del mismo, e identificar problemas para mejorar nuestro Sitio Web. Asimismo, en caso de que preste su consentimiento a trav&eacute;s de su navegaci&oacute;n, utilizaremos cookies que nos permitan obtener m&aacute;s informaci&oacute;n acerca de sus preferencias y personalizar nuestro sitio Web de conformidad con sus intereses individuales.</span></p>\r\n<p dir=\"ltr\"><span>La presente Pol&iacute;tica de Cookies tiene por finalidad informarle de manera clara y precisa sobre las cookies que se utilizan en el sitio Web. En caso de que quiera recabar m&aacute;s informaci&oacute;n sobre las cookies que utilizamos en el sitio Web, podr&aacute; remitir un correo electr&oacute;nico a la siguiente direcci&oacute;n: </span><a href=\"mailto:info@wifibytes.com\"><span>info@wifibytes.com</span></a></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>&iquest;Qu&eacute; son las cookies?</span></strong></p>\r\n<p dir=\"ltr\"><span>Una cookie es un peque&ntilde;o fragmento de texto que los sitios web que visita env&iacute;an al navegador y que permite que el sitio web recuerde informaci&oacute;n sobre su visita, idioma preferido y otras opciones, lo que puede facilitar su pr&oacute;xima visita y hacer que el sitio le resulte m&aacute;s &uacute;til. Las cookies desempe&ntilde;an un papel muy importante y contribuyen a tener una mejor experiencia de usuario.</span></p>\r\n<p dir=\"ltr\"><strong><span>&iquest;Por qu&eacute; son importantes?</span></strong></p>\r\n<p dir=\"ltr\"><span>Las cookies nos permiten hacer del sitio Web de Wifibytes un lugar mejor, m&aacute;s r&aacute;pido y m&aacute;s seguro. Las cookies nos ayudan, por ejemplo, a:</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Habilitar ciertas funciones.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Proporcionar una experiencia de navegaci&oacute;n m&aacute;s personalizada.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Proteger la seguridad de tu cuenta.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Investigar, mejorar y comprender el uso que reciben nuestros productos y servicios.</span>&nbsp;</p>\r\n</li>\r\n</ul>\r\n<p dir=\"ltr\"><span><strong>&iquest;Qu&eacute; tipos de Cookies utiliza Wifibytes</strong>?</span></p>\r\n<p dir=\"ltr\"><span>Cookies de car&aacute;cter t&eacute;cnico y de personalizaci&oacute;n:</span></p>\r\n<p dir=\"ltr\"><span>Son aqu&eacute;llas que nos permiten controlar aspectos relacionados con la sesi&oacute;n del usuario, de tal forma que podemos mejorar la experiencia de navegaci&oacute;n dentro del portal. Por ejemplo:</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Nos permiten reconocer el idioma del usuario.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Impiden o dificultan ataques contra el sitio web o sus usuarios.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Nos ayudan a proporcionar la correcta reproducci&oacute;n de los contenidos multimedia.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Mejoran el rendimiento, permitiendo distribuir el tr&aacute;fico web de nuestras m&aacute;quinas entre varios servidores.</span></p>\r\n</li>\r\n</ul>\r\n<p dir=\"ltr\"><strong><span>&iquest;Qui&eacute;n utiliza las cookies de Wifibytes?</span></strong></p>\r\n<p dir=\"ltr\"><span>A continuaci&oacute;n se muestra el listado de empresas (Terceros), que hacen uso de las cookies:</span></p>\r\n<p><span><span><span>Google Analytics</span><span>, permiten cuantificar el n&uacute;mero de visitantes y analizar estad&iacute;sticamente la utilizaci&oacute;n que hacen los usuarios de nuestros servicios.</span></span></span></p>\r\n<p dir=\"ltr\"><strong><span>Consentimiento</span></strong></p>\r\n<p dir=\"ltr\"><span>Al navegar y continuar en el sitio Web estar&aacute; consintiendo el uso de las cookies antes enunciadas, por los plazos se&ntilde;alados y en las condiciones contenidas en la presente Pol&iacute;tica de Cookies.</span></p>\r\n<p dir=\"ltr\"><span>Puedes obtener m&aacute;s informaci&oacute;n sobre nosotros a trav&eacute;s de nuestro </span><a href=\"http://wifibytes-front.wearecactus.com/#/avisolegal\"><span>aviso legal,</span></a><span> nuestra pol&iacute;tica de privacidad, o mediante correo electr&oacute;nico dirigido a la direcci&oacute;n: </span><span><a href=\"mailto:info@wifibytes.com\">info@wifibytes.com</a></span></p>\r\n</div>",
            "image": null,
            "lang": "val"
        },
        {
            "key": "cookies",
            "content": "<div class=\"ng-binding\">\r\n<p dir=\"ltr\"><strong><span>Pol&iacute;tica de Cookies en Wifibytes</span></strong></p>\r\n<p dir=\"ltr\"><span>Nuestro sitio web </span><a href=\"http://wifibytes-front.wearecactus.com/#/\"><span>http://wifibytes-front.wearecactus.com/#/</span></a><span> utiliza una tecnolog&iacute;a denominada &ldquo;cookies&rdquo;, con la finalidad de poder recabar informaci&oacute;n acerca del uso del sitio web.</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><span>Le informamos que vamos a utilizar cookies con la finalidad de facilitar su navegaci&oacute;n a trav&eacute;s del sitio Web, distinguirle de otros usuarios, proporcionarle una mejor experiencia en el uso del mismo, e identificar problemas para mejorar nuestro Sitio Web. Asimismo, en caso de que preste su consentimiento a trav&eacute;s de su navegaci&oacute;n, utilizaremos cookies que nos permitan obtener m&aacute;s informaci&oacute;n acerca de sus preferencias y personalizar nuestro sitio Web de conformidad con sus intereses individuales.</span></p>\r\n<p dir=\"ltr\"><span>La presente Pol&iacute;tica de Cookies tiene por finalidad informarle de manera clara y precisa sobre las cookies que se utilizan en el sitio Web. En caso de que quiera recabar m&aacute;s informaci&oacute;n sobre las cookies que utilizamos en el sitio Web, podr&aacute; remitir un correo electr&oacute;nico a la siguiente direcci&oacute;n: </span><a href=\"mailto:info@wifibytes.com\"><span>info@wifibytes.com</span></a></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>&iquest;Qu&eacute; son las cookies?</span></strong></p>\r\n<p dir=\"ltr\"><span>Una cookie es un peque&ntilde;o fragmento de texto que los sitios web que visita env&iacute;an al navegador y que permite que el sitio web recuerde informaci&oacute;n sobre su visita, idioma preferido y otras opciones, lo que puede facilitar su pr&oacute;xima visita y hacer que el sitio le resulte m&aacute;s &uacute;til. Las cookies desempe&ntilde;an un papel muy importante y contribuyen a tener una mejor experiencia de usuario.</span></p>\r\n<p dir=\"ltr\"><strong><span>&iquest;Por qu&eacute; son importantes?</span></strong></p>\r\n<p dir=\"ltr\"><span>Las cookies nos permiten hacer del sitio Web de Wifibytes un lugar mejor, m&aacute;s r&aacute;pido y m&aacute;s seguro. Las cookies nos ayudan, por ejemplo, a:</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Habilitar ciertas funciones.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Proporcionar una experiencia de navegaci&oacute;n m&aacute;s personalizada.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Proteger la seguridad de tu cuenta.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Investigar, mejorar y comprender el uso que reciben nuestros productos y servicios.</span>&nbsp;</p>\r\n</li>\r\n</ul>\r\n<p dir=\"ltr\"><span><strong>&iquest;Qu&eacute; tipos de Cookies utiliza Wifibytes</strong>?</span></p>\r\n<p dir=\"ltr\"><span>Cookies de car&aacute;cter t&eacute;cnico y de personalizaci&oacute;n:</span></p>\r\n<p dir=\"ltr\"><span>Son aqu&eacute;llas que nos permiten controlar aspectos relacionados con la sesi&oacute;n del usuario, de tal forma que podemos mejorar la experiencia de navegaci&oacute;n dentro del portal. Por ejemplo:</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<ul>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Nos permiten reconocer el idioma del usuario.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Impiden o dificultan ataques contra el sitio web o sus usuarios.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Nos ayudan a proporcionar la correcta reproducci&oacute;n de los contenidos multimedia.</span></p>\r\n</li>\r\n<li dir=\"ltr\">\r\n<p dir=\"ltr\"><span>- Mejoran el rendimiento, permitiendo distribuir el tr&aacute;fico web de nuestras m&aacute;quinas entre varios servidores.</span></p>\r\n</li>\r\n</ul>\r\n<p dir=\"ltr\"><strong><span>&iquest;Qui&eacute;n utiliza las cookies de Wifibytes?</span></strong></p>\r\n<p dir=\"ltr\"><span>A continuaci&oacute;n se muestra el listado de empresas (Terceros), que hacen uso de las cookies:</span></p>\r\n<p><span><span><span>Google Analytics</span><span>, permiten cuantificar el n&uacute;mero de visitantes y analizar estad&iacute;sticamente la utilizaci&oacute;n que hacen los usuarios de nuestros servicios.</span></span></span></p>\r\n<p dir=\"ltr\"><strong><span>Consentimiento</span></strong></p>\r\n<p dir=\"ltr\"><span>Al navegar y continuar en el sitio Web estar&aacute; consintiendo el uso de las cookies antes enunciadas, por los plazos se&ntilde;alados y en las condiciones contenidas en la presente Pol&iacute;tica de Cookies.</span></p>\r\n<p dir=\"ltr\"><span>Puedes obtener m&aacute;s informaci&oacute;n sobre nosotros a trav&eacute;s de nuestro </span><a href=\"http://wifibytes-front.wearecactus.com/#/avisolegal\"><span>aviso legal,</span></a><span> nuestra pol&iacute;tica de privacidad, o mediante correo electr&oacute;nico dirigido a la direcci&oacute;n: </span><span><a href=\"mailto:info@wifibytes.com\">info@wifibytes.com</a></span></p>\r\n</div>",
            "image": null,
            "lang": "es"
        },
        {
            "key": "us",
            "content": "<div class=\"ng-binding\">\r\n<p>Wifibytes &eacute;s una empresa prove&iuml;dora d'acc&eacute;s a Internet a trav&eacute;s de fibra &ograve;ptica o mitjans sense fil.</p>\r\n<p>&nbsp;</p>\r\n<p>En l'actualitat ofereix servei de connexi&oacute; a Internet, TV, telefonia fixa i m&ograve;bil en diferents localitats de la prov&iacute;ncia d'Alacant i Val&egrave;ncia.</p>\r\n</div>",
            "image": null,
            "lang": "val"
        },
        {
            "key": "us",
            "content": "<div class=\"ng-binding\">\r\n<p><span><span>Wifibytes es una empresa proveedora de acceso a Internet a trav&eacute;s de fibra &oacute;ptica o medios inal&aacute;mbricos.</span></span></p>\r\n<p><span><span>En la actualidad ofrece servicio de conexi&oacute;n a Internet, TV, telefon&iacute;a fija i m&oacute;vil en diferentes localidades de la provincia de Alicante y Valencia.</span></span></p>\r\n</div>",
            "image": null,
            "lang": "es"
        },
        {
            "key": "legal_advise",
            "content": "<div class=\"ng-binding\">\r\n<p dir=\"ltr\"><strong><span>DADES GENERALS</span></strong></p>\r\n<p dir=\"ltr\"><span>D'acord amb l'article 10 de la Llei 34/2002, de 11 de juliol, de Serveis de la Societat de la Informaci&oacute; i de Comer&ccedil; Electr&ograve;nic posem a la seva disposici&oacute; les seg&uuml;ents dades:</span></p>\r\n<p dir=\"ltr\"><span>Wifibytes SL. est&agrave; domiciliada al carrer C / Batalla de Lepant n&ordm;5, Baix (Bocairent), amb CIF B98137078.Inscrita en el Registre Mercantil de Val&egrave;ncia, Vol 9030, Foli 34, Full V-133.779, Inscripci&oacute; 4&ordf;.</span></p>\r\n<p dir=\"ltr\"><span>Al web http://wifibytes-front.wearecactus.com/#/ hi ha una s&egrave;rie de continguts de car&agrave;cter informatiu sobre http://wifibytes-front.wearecactus.com/#/nosotros</span></p>\r\n<p dir=\"ltr\"><span>El seu principal objectiu &eacute;s facilitar als clients i al p&uacute;blic en general, la informaci&oacute; relativa a l'empresa, als productes i serveis que s'ofereixen.</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>POL&Iacute;TICA DE PRIVACITAT</span></strong></p>\r\n<p dir=\"ltr\"><span>En compliment del que disposa la Llei Org&agrave;nica 15/1999, de 13 de desembre, de protecci&oacute; de dades de car&agrave;cter personal (LOPD) s'informa a l'usuari que totes les dades que ens proporcioni seran incorporades a un fitxer, creat i mantingut sota la responsabilitat de Wifibytes SL</span></p>\r\n<p dir=\"ltr\"><span>Sempre es va a respectar la confidencialitat de les seves dades personals que nom&eacute;s seran utilitzades amb la finalitat de gestionar els serveis oferts, atendre les sol&middot;licituds que ens plantegi, realitzar tasques administratives, aix&iacute; com remetre informaci&oacute; t&egrave;cnica, comercial o publicit&agrave;ria per via ordin&agrave;ria o electr&ograve;nica .</span></p>\r\n<p dir=\"ltr\"><span>Per exercir els seus drets d'oposici&oacute;, rectificaci&oacute; o cancel&middot;laci&oacute; s'ha de dirigir a la seu de l'empresa C / Batalla de Lepant, n&ordm;5 Baix (Bocairent), escriure'ns al seg&uuml;ent correu info@wifibytes.com o truca'ns al 960.500.606</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>CONDICIONS D'&Uacute;S</span></strong></p>\r\n<p dir=\"ltr\"><span>Les condicions d'acc&eacute;s i &uacute;s del present lloc web es regeixen per la legalitat vigent i pel principi de bona fe comprometent-se l'usuari a realitzar un bon &uacute;s de la web. No es permeten conductes que vagin contra la llei, els drets o interessos de tercers.</span></p>\r\n<p dir=\"ltr\"><span>Ser usuari del web de http://wifibytes.wearecactus.com/front/#/ implica que reconeix haver llegit i acceptat les presents condicions i el que les estengui la normativa legal aplicable en aquesta mat&egrave;ria. Si pel motiu que sigui no est&agrave; d'acord amb aquestes condicions no continu&iuml; usant aquest web.</span></p>\r\n<p dir=\"ltr\"><span>Qualsevol tipus de notificaci&oacute; i / o reclamaci&oacute; solament ser&agrave; v&agrave;lida per notificaci&oacute; escrita i / o correu certificat.</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>RESPONSABILITATS</span></strong></p>\r\n<p dir=\"ltr\"><span>Wifibytes SL no es fa responsable de la informaci&oacute; i continguts emmagatzemats en f&ograve;rums, xarxes socials o qualssevol altre mitj&agrave; que permeti a tercers publicar continguts de forma independent en la p&agrave;gina web del prestador.</span></p>\r\n<p dir=\"ltr\"><span>No obstant aix&ograve;, tenint en compte els art. 11 i 16 de la LSSI-CE, Wifibytes SL es compromet a la retirada o si escau bloqueig d'aquells continguts que puguin afectar o contravenir la legislaci&oacute; nacional o internacional, drets de tercers o la moral i l'ordre p&uacute;blic.</span></p>\r\n<p dir=\"ltr\"><span>Tampoc l'empresa es far&agrave; responsable dels danys i perjudicis que es produeixin per fallades o males configuracions del programari instal&middot;lat a l'ordinador de l'internauta. S'exclou tota responsabilitat per alguna incid&egrave;ncia t&egrave;cnica o fallada que es produeixi quan l'usuari es connecti a internet. Igualment no es garanteix la inexist&egrave;ncia d'interrupcions o errors en l'acc&eacute;s al lloc web.</span></p>\r\n<p dir=\"ltr\"><span>Aix&iacute; mateix Wifibytes SL es reserva el dret a actualitzar, modificar o eliminar la informaci&oacute; continguda en la seva p&agrave;gina web, aix&iacute; com la configuraci&oacute; o presentaci&oacute; del mateix, en qualsevol moment sense assumir alguna responsabilitat per aix&ograve;.</span></p>\r\n<p dir=\"ltr\"><span>Li comuniquem que qualsevol preu que pugui veure a la nostra web ser&agrave; nom&eacute;s orientatiu. Si l'usuari desitja saber amb exactitud el preu o si el producte en el moment actual compta amb alguna oferta de la qual es pot beneficiar s'ha d'acudir a alguna de les botigues f&iacute;siques amb qu&egrave; compta Wifibytes SL.</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>PROPIETAT INTEL&middot;LECTUAL I INDUSTRIAL</span></strong></p>\r\n<p dir=\"ltr\"><span>Wifibytes SL. &eacute;s titular de tots els drets sobre el programari de la publicaci&oacute; digital aix&iacute; com dels drets de propietat industrial i intel&middot;lectual referits als continguts que s'hi incloguin, a excepci&oacute; dels drets sobre productes i serveis de car&agrave;cter p&uacute;blic que no s&oacute;n propietat d'aquesta empresa .</span></p>\r\n<p dir=\"ltr\"><span>Cap material publicat en aquesta web podr&agrave; ser reprodu&iuml;t, copiat o publicat sense el consentiment per escrit de Wifibytes SL.</span></p>\r\n<p dir=\"ltr\"><span>Tota la informaci&oacute; que es rebi a la web, com comentaris, suggeriments o idees, es considerar&agrave; cedida a Wifibytes SL.de manera gratu&iuml;ta. No s'ha d'enviar informaci&oacute; que NO pugui ser tractada d'aquesta manera.</span></p>\r\n<p dir=\"ltr\"><span>Tots els productes i serveis d'aquestes p&agrave;gines que NO s&oacute;n ​​propietat de Wifibytes SL. s&oacute;n marques registrades dels seus respectius propietaris i s&oacute;n reconegudes com a tals per la nostra empresa. Nom&eacute;s apareixen al web de Wifibytes SL. a efectes de promoci&oacute; i de recopilaci&oacute; d'informaci&oacute;. Aquests propietaris poden sol&middot;licitar la modificaci&oacute; o eliminaci&oacute; de la informaci&oacute; que els pertany.</span></p>\r\n<p dir=\"ltr\">&nbsp;</p>\r\n<p dir=\"ltr\"><strong><span>LLEI APLICABLE I JURISDICCIO</span></strong></p>\r\n<p dir=\"ltr\"><span>Les presents condicions generals es regeixen per la legislaci&oacute; espanyola. Per a qualsevol litigi que pogu&eacute;s sorgir relacionat amb el lloc web o l'activitat que en ell es desenvolupa seran competents Jutjats d'Ontinyent, renunciant expressament l'usuari a qualsevol altre fur que pogu&eacute;s correspondre-li.</span></p>\r\n</div>",
            "image": null,
            "lang": "val"
        }
    ]
};

endpoint.productos = {
    "count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "referencia": "4c4dcf1f-8ac6-4d50-bc5e-d9266377848d",
            "templates": {
                "template3": {
                    "pretitulo": "",
                    "franja_1_texto": "",
                    "franja_1_fondo": null,
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "franja_2_texto": "",
                    "franja_2_fondo": null,
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "articulo": null,
                    "idioma": null
                },
                "template2": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                },
                "template1": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                }
            },
            "descripcion": "BQ Aquaris X",
            "descripcion_breve": "Diseño superior.",
            "descripcion_larga": "<div class=\"col-sm-6\">\r\n<h4 class=\"title-caja01 ng-binding\">Dise&ntilde;o superior.</h4>\r\n<p>Una pantalla que abraza sutilmente el marco met&aacute;lico, con l&iacute;neas limpias y bordes ovalados gracias al cristal 2,5D. Bordes reducidos y una parte trasera de cristal 3D en la que hemos seguido un cuidado proceso de pulido y esmaltado de alt&iacute;sima precisi&oacute;n consiguiendo un tacto tan suave que no podr&aacute;s dejar de tocarlo</p>\r\n<h4 class=\"ng-binding\">Lluvia de colores.</h4>\r\n<p>La tecnolog&iacute;a Quantum Color + nunca hab&iacute;a brillado tanto. Disfruta de m&aacute;s de 16,5 millones de colores en una pantalla de 5,2&rdquo; con resoluci&oacute;n FHD y un brillo de hasta 650 nits. Su pantalla LTPS es a&uacute;n m&aacute;s fina y eficiente y gracias a la tecnolog&iacute;a in-cell, que combina el LCD con la capa t&aacute;ctil, ofrece un &aacute;ngulo de visi&oacute;n mayor. El resultado: im&aacute;genes espectaculares con el m&aacute;ximo realismo.</p>\r\n</div>",
            "imagen": "http://127.0.0.1:8000/media/pagina_tarifas/aq_x_01_Sq6cFne.png",
            "thumbnail": "http://127.0.0.1:8000/media/pagina_tarifas/aq_x_01_Sq6cFne_pHU7Je0.png",
            "slug": "bq-aquaris-x",
            "descripcion_va": "BQ Aquaris X",
            "pvp": 300.0,
            "stockfis": 6.0,
            "descripcion_breve_va": "Disseny superior.",
            "descripcion_larga_va": "<h4 class=\"ng-binding\">Mejor autonom&iacute;a..</h4>\r\n<p>BQ es sin&oacute;nimo de autonom&iacute;a y con Aquaris X Pro lo es m&aacute;s que nunca. Su bater&iacute;a de 3.100 mAh, combinada con componentes de bajo consumo, ofrece hasta un 20% m&aacute;s de autonom&iacute;a que modelos anteriores. Podr&aacute;s navegar durante m&aacute;s de 10 horas o ver tu serie favorita durante m&aacute;s de 11 horas seguidas . Y gracias a la funci&oacute;n Quick Charge 3.0, carga hasta un 50% de bater&iacute;a en s&oacute;lo 30 minutos.</p>",
            "template": 1,
            "activo": true,
            "visible": true,
            "destacado": true,
            "secompra": true,
            "stockmax": 0.0,
            "codimpuesto": "IVA21%",
            "observaciones": "",
            "codbarras": "",
            "nostock": false,
            "controlstock": false,
            "tipocodbarras": null,
            "sevende": true,
            "venta_online": true,
            "stockmin": 0.0,
            "created_at": 1542628976,
            "updated_at": 1542628976,
            "codfamilia": "Tlf",
            "marca": 1,
            "pantalla": 1,
            "procesador": 1,
            "ram": 1,
            "camara": 1
        },
        {
            "referencia": "685ca68b-2ca1-4943-879e-068f8f96d668",
            "templates": {
                "template3": {
                    "pretitulo": "",
                    "franja_1_texto": "",
                    "franja_1_fondo": null,
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "franja_2_texto": "",
                    "franja_2_fondo": null,
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "articulo": null,
                    "idioma": null
                },
                "template2": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                },
                "template1": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                }
            },
            "descripcion": "Xiaomi Redmi Note 4",
            "descripcion_breve": "Xiaomi Redmi Note 4",
            "descripcion_larga": "<div class=\"col-sm-6\">\r\n<h4 class=\"title-caja01 ng-binding\">Evoluci&oacute;n.</h4>\r\n<p><span>Toda la parte trasera de Redmi Note 4 es de aluminio . Este noble metal tambi&eacute;n ha llegado al&nbsp;bot&oacute;n&nbsp;de encendido y el control de volumen que en esta edici&oacute;n son de aluminio</span></p>\r\n<h4 class=\"ng-binding\">Rendimiento.</h4>\r\n<p><span>El Note 4 tiene uno de los mejores rendimientos de la saga. Con un Mediatek Helio X20 y</span><span>&nbsp;junto a una GPU Mali-T880 MP4 es una excelente opci&oacute;n paro los que nos gusta jugar.</span></p>\r\n<h4 class=\"ng-binding\">Pura energ&iacute;a.</h4>\r\n<p><span>La bater&iacute;a con 4100 mAh tiene de las mejores autonom&iacute;as que hay actualmente en el mercado. La autonom&iacute;a soporta unas 5 horas de pantalla y llega a las 24 horas de autonom&iacute;a, aunque el d&iacute;a sea muy duro. Si el uso no es muy escesivo el dispositivo aguanta despierto m&aacute;s de 36 horas.</span></p>\r\n</div>",
            "imagen": "http://127.0.0.1:8000/media/pagina_tarifas/redmi_note_4_01.png",
            "thumbnail": "http://127.0.0.1:8000/media/pagina_tarifas/redmi_note_4_01_tKCpLpi.png",
            "slug": "xiaomi-redmi-note-4",
            "descripcion_va": "Xiaomi Redmi Note 4",
            "pvp": 315.0,
            "stockfis": 4.0,
            "descripcion_breve_va": "Xiaomi Redmi Note 4",
            "descripcion_larga_va": "<p>Lorem Ipsum</p>",
            "template": 2,
            "activo": true,
            "visible": true,
            "destacado": true,
            "secompra": true,
            "stockmax": 0.0,
            "codimpuesto": "IVA21%",
            "observaciones": "",
            "codbarras": "",
            "nostock": false,
            "controlstock": false,
            "tipocodbarras": null,
            "sevende": true,
            "venta_online": true,
            "stockmin": 0.0,
            "created_at": 1542629256,
            "updated_at": 1542644716,
            "codfamilia": "Tlf",
            "marca": 2,
            "pantalla": 2,
            "procesador": 1,
            "ram": 1,
            "camara": 2
        },
        {
            "referencia": "862647ba-092a-44ba-9007-f9a2be5ec188",
            "templates": {
                "template3": {
                    "pretitulo": "",
                    "franja_1_texto": "",
                    "franja_1_fondo": null,
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "franja_2_texto": "",
                    "franja_2_fondo": null,
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "articulo": null,
                    "idioma": null
                },
                "template2": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                },
                "template1": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                }
            },
            "descripcion": "BQ Aquaris X pro",
            "descripcion_breve": "BQ Aquaris X pro",
            "descripcion_larga": "<div class=\"col-sm-6\">\r\n<div class=\"col-sm-6\">\r\n<h4 class=\"title-caja01 ng-binding\">Obra de arte.</h4>\r\n<p>Una pantalla que abraza sutilmente el marco met&aacute;lico, con l&iacute;neas limpias y bordes ovalados gracias al cristal 2,5D. Bordes reducidos y una parte trasera de cristal 3D en la que hemos seguido un cuidado proceso de pulido y esmaltado de alt&iacute;sima precisi&oacute;n consiguiendo un tacto tan suave que no podr&aacute;s dejar de tocarlo</p>\r\n<div class=\"col-sm-6\">\r\n<h4 class=\"ng-binding\">M&aacute;ximo color.</h4>\r\n<p>La tecnolog&iacute;a Quantum Color + nunca hab&iacute;a brillado tanto. Disfruta de m&aacute;s de 16,5 millones de colores en una pantalla de 5,2&rdquo; con resoluci&oacute;n FHD y un brillo de hasta 650 nits. Su pantalla LTPS es a&uacute;n m&aacute;s fina y eficiente y gracias a la tecnolog&iacute;a in-cell, que combina el LCD con la capa t&aacute;ctil, ofrece un &aacute;ngulo de visi&oacute;n mayor. El resultado: im&aacute;genes espectaculares con el m&aacute;ximo realismo.</p>\r\n<h4 class=\"ng-binding\">Eficiencia en&eacute;rgetica.</h4>\r\n<p>BQ es sin&oacute;nimo de autonom&iacute;a y con Aquaris X Pro lo es m&aacute;s que nunca. Su bater&iacute;a de 3.100 mAh, combinada con componentes de bajo consumo, ofrece hasta un 20% m&aacute;s de autonom&iacute;a que modelos anteriores. Podr&aacute;s navegar durante m&aacute;s de 10 horas o ver tu serie favorita durante m&aacute;s de 11 horas seguidas . Y gracias a la funci&oacute;n Quick Charge 3.0, carga hasta un 50% de bater&iacute;a en s&oacute;lo 30 minutos.</p>\r\n</div>\r\n</div>\r\n</div>",
            "imagen": "http://127.0.0.1:8000/media/pagina_tarifas/aq_x_01_Sq6cFne_fssrFAL.png",
            "thumbnail": "http://127.0.0.1:8000/media/pagina_tarifas/aq_x_01_Sq6cFne_NWzagcp.png",
            "slug": "bq-aquaris-x-pro",
            "descripcion_va": "BQ Aquaris X pro",
            "pvp": 370.0,
            "stockfis": 4.0,
            "descripcion_breve_va": "BQ Aquaris X pro",
            "descripcion_larga_va": "<p>Lorem Ipsum</p>",
            "template": 3,
            "activo": true,
            "visible": true,
            "destacado": true,
            "secompra": true,
            "stockmax": 0.0,
            "codimpuesto": "IVA21%",
            "observaciones": "",
            "codbarras": "",
            "nostock": false,
            "controlstock": false,
            "tipocodbarras": null,
            "sevende": true,
            "venta_online": true,
            "stockmin": 0.0,
            "created_at": 1542646162,
            "updated_at": 1542646162,
            "codfamilia": "Tlf",
            "marca": 1,
            "pantalla": 1,
            "procesador": 1,
            "ram": 2,
            "camara": 1
        },
        {
            "referencia": "6cef527b-5e7f-4369-b3b8-17c70b587364",
            "templates": {
                "template3": {
                    "pretitulo": "",
                    "franja_1_texto": "",
                    "franja_1_fondo": null,
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "franja_2_texto": "",
                    "franja_2_fondo": null,
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "articulo": null,
                    "idioma": null
                },
                "template2": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen4": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                },
                "template1": {
                    "pretitulo": "",
                    "caja_1_titulo": "",
                    "caja_1_texto": "",
                    "caja_2_titulo": "",
                    "caja_2_texto": "",
                    "caja_3_titulo": "",
                    "caja_3_texto": "",
                    "caja_4_titulo": "",
                    "caja_4_texto": "",
                    "imagen1": null,
                    "imagen2": null,
                    "imagen3": null,
                    "imagen_fondo_cabecera": null,
                    "imagen_fondo_cuerpo": null,
                    "articulo": null,
                    "idioma": null
                }
            },
            "descripcion": "BQ A 4.5",
            "descripcion_breve": "BQ A 4.5",
            "descripcion_larga": "<div class=\"col-sm-6\">\r\n<h4 class=\"title-caja01 ng-binding\">Acercamos la tecnologia..</h4>\r\n<p><span>Nuestra filosof&iacute;a es hacer la tecnolog&iacute;a accesible al mundo para acercarla a las personas. Por ello hemos trabajado junto con Google para traer el proyecto Android One&trade; a Espa&ntilde;a y Portugal. El resultado es Aquaris A4.5, un dispositivo con un hardware de &uacute;ltima generaci&oacute;n y todas las ventajas de Android One.</span></p>\r\n<div class=\"col-sm-6\">\r\n<h4 class=\"ng-binding\">Ahora, con Android 6 Marshmallow..</h4>\r\n<p>Al formar parte de la familia Android One, Aquaris A4.5 es de los primeros smartphones en actualizarse a las nuevas versiones. Por eso, ya puedes disfrutar en &eacute;l de Android 6 Marshmallow. Descubre todas las funcionalidades de Android puro, mantente siempre al d&iacute;a y experimenta la mejor optimizaci&oacute;n de las aplicaciones y servicios.</p>\r\n<h4 class=\"ng-binding\">El mejor hardware y software para tu pantalla..</h4>\r\n<p>La pantalla de Aquaris A4.5 es mucho m&aacute;s de lo que ves. Es resistente ante impactos y ara&ntilde;azos gracias a la protecci&oacute;n Dragontrail.</p>\r\n<p>Mantiene su sensibilidad t&aacute;ctil en ambientes h&uacute;medos e incluso con guantes. Tambi&eacute;n hemos incluido la funci&oacute;n double tap, para bloquear y encender la pantalla con dos leves toques.</p>\r\n</div>\r\n</div>",
            "imagen": "http://127.0.0.1:8000/media/pagina_tarifas/a4.5_03_HYSMy9E.png",
            "thumbnail": "http://127.0.0.1:8000/media/pagina_tarifas/a4.5_03_HYSMy9E_iQ2rPi0.png",
            "slug": "bq-a-45",
            "descripcion_va": "BQ A 4.5",
            "pvp": 175.0,
            "stockfis": 50.0,
            "descripcion_breve_va": "BQ A 4.5",
            "descripcion_larga_va": "<p>Lorem Ipsum</p>",
            "template": 2,
            "activo": true,
            "visible": true,
            "destacado": true,
            "secompra": true,
            "stockmax": 0.0,
            "codimpuesto": "IVA21%",
            "observaciones": "",
            "codbarras": "",
            "nostock": false,
            "controlstock": false,
            "tipocodbarras": null,
            "sevende": true,
            "venta_online": true,
            "stockmin": 0.0,
            "created_at": 1542646491,
            "updated_at": 1542646491,
            "codfamilia": "Tlf",
            "marca": 1,
            "pantalla": 3,
            "procesador": 2,
            "ram": 3,
            "camara": 3
        }
    ]
};
endpoint.filtros = {
    "pantalla": [
        {
            "id": 1,
            "num_pantalla": 5.2
        },
        {
            "id": 2,
            "num_pantalla": 5.5
        },
        {
            "id": 3,
            "num_pantalla": 4.5
        }
    ],
    "procesador": [
        {
            "id": 1,
            "num_procesador": "Snapdragon 626"
        },
        {
            "id": 2,
            "num_procesador": "MediaTek MT6735M"
        }
    ],
    "ram": [
        {
            "id": 1,
            "num_ram": "3GB"
        },
        {
            "id": 2,
            "num_ram": "4GB"
        },
        {
            "id": 3,
            "num_ram": "1GB"
        }
    ],
    "marca": [
        {
            "id": 1,
            "Marca": "Bq"
        },
        {
            "id": 2,
            "Marca": "Xiaomi"
        }
    ],
    "camara": [
        {
            "id": 1,
            "num_camara": 16.0
        },
        {
            "id": 2,
            "num_camara": 13.0
        },
        {
            "id": 3,
            "num_camara": 8.0
        }
    ]
};
export {endpoint};
