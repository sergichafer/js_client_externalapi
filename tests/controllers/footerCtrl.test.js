const $ = require('jquery');
import {FooterController, footer_options} from '../../src/controllers/footerCtrl';
import {endpoint} from '../endpoints';
beforeEach(() => {
  // Set up our document body
  document.body.innerHTML = `
    <html lang="en">
        <head>
            <meta http-equiv="content-type" content="text/html; charset=utf-8">
            <title>title</title>
            <link rel="stylesheet" type="text/css" href="main.css">
            <script type="text/javascript" src="main.js"></script>
        </head>
        <body>
            <header id="nav" class="flexContainer--color-greyBackground"></header>
            
            <section class="flexContainer flexContainer--position-Center flexContainer--size-fullHeight flexContainer--color-whiteBackground">
                <aside class="sidebar sidebar--position-Left"></aside>
                <main id="main" class="flexContainer--color-whiteBackground main"></main>
                <aside class="sidebar sidebar--position-Right"></aside>
            </section>
            
            <section id="section"></section>
            <footer id="footer" class="footer flexContainer flexContainer--position-Center flexContainer--color-greyBackground"></footer>
            
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWYCfA-9ILXja6UwCg8SJw02L75Kqo_nQ"></script>
        </body>
    </html>`;
  fakeDOMLoaded();
});

test('Footer template is printed with data from an endpoint /datos_empresa', () => {
  new FooterController(endpoint.datos_empresa, "#footer", 'es');
  expect(document.body.toString().search("{")).toBe(-1);
  expect($('#facebook').attr('href')).toBe("https://twitter.com/wifibytes");
  expect($('#twitter').attr('href')).toBe("https://www.facebook.com/wifibytes");
});

test('Us, Legal, Cookies template is printed with data from an endpoint /datos_empresa', () => {
  new footer_options(endpoint.datos_empresa, "#main", "us", 'es');
  expect(document.body.toString().search("{")).toBe(-1);
  expect(($('#main').is(':empty'))).toBeFalsy();

  new footer_options(endpoint.datos_empresa, "#main", "legal_advise", 'es');
  expect(document.body.toString().search("{")).toBe(-1);
  expect(($('#main').is(':empty'))).toBeFalsy();

  new footer_options(endpoint.datos_empresa, "#main", "cookies", 'es');
  expect(document.body.toString().search("{")).toBe(-1);
  expect(($('#main').is(':empty'))).toBeFalsy();
});

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');
  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}