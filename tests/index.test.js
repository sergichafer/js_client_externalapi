const $ = require('jquery');
import {HomeController, SectionController} from '../src/controllers/homeCtrl';
import NavController from '../src/controllers/navCtrl';
import {FooterController} from '../src/controllers/footerCtrl';
import {endpoint} from './endpoints';


beforeEach(() => {
  // Set up our document body
  document.body.innerHTML = `
    <html lang="en">
        <head>
            <meta http-equiv="content-type" content="text/html; charset=utf-8">
            <title>title</title>
            <link rel="stylesheet" type="text/css" href="main.css">
            <script type="text/javascript" src="main.js"></script>
        </head>
        <body>
            <header id="nav" class="flexContainer--color-greyBackground"></header>
            
            <section class="flexContainer flexContainer--position-Center flexContainer--size-fullHeight flexContainer--color-whiteBackground">
                <aside class="sidebar sidebar--position-Left"></aside>
                <main id="main" class="flexContainer--color-whiteBackground main"></main>
                <aside class="sidebar sidebar--position-Right"></aside>
            </section>
            
            <section id="section"></section>
            <footer id="footer" class="footer flexContainer flexContainer--position-Center flexContainer--color-greyBackground"></footer>
            
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWYCfA-9ILXja6UwCg8SJw02L75Kqo_nQ"></script>
        </body>
    </html>`;
  fakeDOMLoaded();
});

it('Test if index.js function onInit works', () => {

  new NavController(endpoint.datos_empresa, "#main", 'es');
  expect($('#company_logo').attr('src')).toBe("http://127.0.0.1:8000/media/logo/3ad95f57-18f8-4406-ab37-13b472a8598a.png");

  new HomeController(endpoint.home, "#main", 'es');
  expect($('#titulo').text()).toBe("Wifibytes");
  expect($('#subtitulo').text()).toBe("Otra forma de hacer comunicación");

  new SectionController(endpoint.tarifa, "#section", 3, "No");
  expect($('.logo:first').attr('src')).toBe("http://127.0.0.1:8000/media/Logo/icon-egg_FRpw391.png");
  expect($('.nombretarifa:first').text()).toBe("Egg");
  expect($('.logo:last').attr('src')).toBe("http://127.0.0.1:8000/media/Logo/chick_8qOw42B.png");
  expect($('.nombretarifa:last').text()).toBe("Chick");

  new FooterController(endpoint.datos_empresa, "#footer", 'es');
  expect($('#facebook').attr('href')).toBe("https://twitter.com/wifibytes");
  expect($('#twitter').attr('href')).toBe("https://www.facebook.com/wifibytes");

});

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');
  
  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}