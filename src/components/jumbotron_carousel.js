import {get} from '../utils';
import {Settings} from '../settings';
import {template, slide, controls} from './carousel_template';

/**
 * @class
 * @classdesc This class represents a Carousel component.
 */    
class JumbotronCarousel extends HTMLElement {
/**
 * @constructor
 */    
    constructor() {
        super();
    }
    /** connectedCallback  */
    connectedCallback() {
    /** 
     * @method connectedCallback 
     */
        var shadow = this.attachShadow({mode: 'open'});
        this.startCarousel(shadow);
        this.addSlides(shadow);
    }
    /** startCarousel  */
    startCarousel(shadow) {
    /**
     * Represents a Carousel component mehtod.
     * @method startCarousel 
     * single type @param {HTMLElement} shadow
     */
        shadow.innerHTML = this.template();
        const styleTag = document.createElement('style');
        styleTag.textContent = getStyle();
        shadow.appendChild(styleTag);
    }

    template(){
        return `
        <div class="slideshow">
          <div class="slide-wrapper" id="items">
          </div>
        </div>`;
    }

    /** addSlides  */
    addSlides(shadow){
        /**
        * Represents a Carousel component mehtod.
        * @method startCarousel 
        * single type @param {HTMLElement} shadow
        */
        get(Settings.baseURL+'/datos_empresa').then(function(response) {           
            let datosEmpresa=JSON.parse(response);
            /**
             * Filters "textos" from datos_empresa endpoint where the key matches regex /jumbotron/.
             */
            datosEmpresa = datosEmpresa.textos.filter(function (entry) { return /jumbotron/.test(entry.key); });
            /**
             * Each element resulting is printed into shadow htmlElement id: #items
             */
            datosEmpresa.forEach(function(element, index) {
                try{shadow.querySelector('#items').innerHTML +=slide(element, index);}catch(e){console.log("error")};
            });
        }).catch(function(error) {
            console.log("Failed!", error);
        });
    }
    
}

/**
 * @function getStyle
 * Returns style for the shadow element
 */
/** getStyle  */
const getStyle = () => {
    return `
    .slideshow {
        overflow: hidden;
        height: 510px;
        width: 728px;
        margin: 0 auto;
      }
      
      .slide-wrapper {
        width: 2912px;
        -webkit-animation: slide 12s ease infinite;
      }
      
      .slide {
        float: left;
        height: 510px;
        width: 728px;
      }
      
      .slide-number {
        color: #000;
        text-align: center;
        font-size: 10em;
      }
      
      @-webkit-keyframes slide {
        20% {margin-left: 0px;}
        30% {margin-left: -728px;}
        50% {margin-left: -728px;}
        60% {margin-left: -1456px;}
        70% {margin-left: -1456px;}
        80% {margin-left: -2184px;}
        90% {margin-left: -2184px;}
      }
      
`;
}

export {JumbotronCarousel};
    
  