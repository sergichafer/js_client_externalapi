let template = `
<div class="slideshow">
  <div class="slide-wrapper" id="items">
  </div>
</div>`;

let slide = (data, index) => `
<div class="slide" style="background: url(${data.image}); background-size: 728px 510px;">
    <p>${data.content}</p>
</div>
`;

let controls =  `
<div class="button-holder">
    <a href="#slider-image-1" class="slider-change"></a>
    <a href="#slider-image-2" class="slider-change"></a>
    <a href="#slider-image-3" class="slider-change"></a>
</div>
`;
export {template, slide, controls};