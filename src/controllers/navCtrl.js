import Component from './component';
import {languages_available} from '../lib/languages_available';
/** 
 * @class 
 * This class is used as a navigation bar controller
 */
class NavController extends Component {
    /** 
     * @constructor
     */
    constructor(data, whereTo, lang) {
        super(data, whereTo, lang);
        this.text = languages_available[lang];
        this.text = require('../lib/i18n/'+this.text+'.json');
        try {
            document.querySelector(whereTo).innerHTML = this.render(data);
        }catch(e){
            console.log("error")
        };
    }
  
    /** render  */
    render(data) {
    /** 
     * @method render 
     * Prints navigation bar data
     */
 
        return `<nav class="flexContainer">
            <ul class="nav footer--position-mid">
                <a href="#home">
                    <li>
                        <img id="company_logo" src="${data.logo}" height="80" width="80"/>
                    </li>
                </a>
            </ul>

            <ul id="menu" class="nav footer--position-mid flexContainer">
                <li><a href="#home">${this.text.home}</a></li>
                <li><a href="#tariff">${this.text.rates}</a></li>
                <li><a href="#products">${this.text.products}</a></li>
                <li><a href="#us">${this.text.about}</a></li>
                <li><a href="#contacte">${this.text.contact}</a></li>
            </ul>
        </nav>`
    }
}

export default NavController;