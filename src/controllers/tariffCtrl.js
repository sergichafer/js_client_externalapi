import Component from './component';
/**
 * @class
 * This controller renders data for the tariff page
 */  
class TariffController extends Component {
    /**
     * tariff component constructor.
     * @constructor
     */  
    constructor(whereTo) {
        super(whereTo);
        /** Innering html */
        try{
            document.querySelector(whereTo).innerHTML = this.render();
        }catch(e){
            console.log("error")
        };
    }
    
    /** render  */
    render() {
        return `<section id="tariff" class="main--parent">
            <article class="main--position-full">
                <h1 id="titulo">Tarification</h1>
                <h2 id="subtitulo">Check out our offers</h2>
            </article>
            <section id="section" class="main--position-full"></section>
        </section>`;
    }
}

export default TariffController;