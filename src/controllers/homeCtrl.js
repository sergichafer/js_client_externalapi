import Component from './component';

/**
 * @class
 * This controller renders data for the home page
 * Inners enterprise date, tarfication data and a component
 */  
class HomeController extends Component {
/**
 * Carousel component constructor.
 * @constructor
 */  
    constructor(data, whereTo, lang) {
        super(data, whereTo, lang);
        let results = data.filter(function (entry) { return entry.lang === lang; });
        /** Template data passed while innering html */
        try{
            document.querySelector(whereTo).innerHTML = this.render(results[0]);
        }catch(e){
            console.log("error")
        };
    }
  
    /** render  */
    render(data) {
        return `<section id="homepage" class="main--parent">
            <article class="main--position-full">
                <h1 id="titulo">${data.titulo}</h1>
                <h2 id="subtitulo">${data.subtitulo}</h3>
            </article>
            <section class="main--position-full">
                <jumbotron-carousel></jumbotron-carousel>
            </section>
            <section id="section" class="main--position-full"></section>
            <article class="main--position-mid">
                <h3 id="caja_izquierda_titulo">${data.caja_izquierda_titulo}</h3>
                <p id="caja_izquierda_texto">${data.caja_izquierda_texto}<p>
            </article>
            <article class="main--position-mid">
                <h3 id="caja_derecha_titulo">${data.caja_derecha_titulo}</h3>
                <p id="caja_derecha_texto">${data.caja_derecha_texto}<p>
            </article>
            <article>
            <div class="content-container">
                <video class="fake-background" poster="https://imelgrat.me/demo/images/moth.jpg" id="moth-background" autoplay playsinline muted loop>
                    <source src="https://imelgrat.me/demo/videos/moth.webm" type="video/webm">
                    <source src="https://imelgrat.me/demo/videos/moth.mp4" type="video/mp4">
                </video>
                <div class="fake-overlay"></div>
                <div class="content">
                    <h1>Video background!</h1>
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et risus diam. Mauris placerat dictum ante, condimentum interdum ligula feugiat eget. Ut non ligula mattis turpis imperdiet scelerisque et ut quam. Proin nec diam est. Duis vitae rhoncus nisi. Pellentesque scelerisque eros et orci suscipit commodo. Phasellus varius vel nisl eget mollis. Proin velit sem, euismod nec libero blandit, ultricies vehicula est. Curabitur ac egestas arcu.</p>
                </div>
            </div>
            </article>
        </section>`;
    }
}

class SectionController {
    /**
     * Carousel component constructor.
     * @constructor
     */  
    constructor(data, whereTo, limit, pagination) {
        /**
         * Slices results given by the previous filter into two, this is only for aesthetics in the home page
         */

        try{
            document.querySelector(whereTo).innerHTML = this.starting_point();
        }catch(e){
            console.log("error")
        };

        paginate(data.results, limit, 1);
        
        if (pagination==='Yes'){
            let page_number = data.count/limit;
            document.querySelector("#paginate").innerHTML +=`<section class="main--position-full flexContainer--align-Center">
            <div class="pagination" id="pagination"></div>
            </section>`;
            for (let i = 0; i < page_number; i++) {
                if(i===0){
                    document.querySelector("#pagination").innerHTML += `<a id="pagina${i+1}" class="pagina active">${i+1}</a>`;
                }else{
                    document.querySelector("#pagination").innerHTML += `<a id="pagina${i+1}" class="pagina">${i+1}</a>`;
                }
                
            }   
        }

        const addClickEvent = elem => elem.addEventListener("click", function(event){
            document.querySelector('.active').classList.remove('active');
            document.querySelector("#pagina"+event.target.innerText).setAttribute("class", "active"); 
            paginate(data.results, limit, event.target.innerText);
        });

        document.querySelectorAll(".pagina").forEach(addClickEvent);

        
    }

    starting_point(){
        return `<section id="tarifes" class="main--parent"></section>
        <section id="paginate" class="main--position-full"></section>
        `;
    }

    
}


function paginate(array, page_size, page_number){
    --page_number; // because pages logically start with 1, but technically with 0
    array.slice(page_number * page_size, (page_number + 1) * page_size).forEach(function(element, index) {
        let options = ``;
        if(element.subtarifas[0].subtarifa_datos_internet){
            options += `<li>${element.subtarifas[0].subtarifa_datos_internet} Mb/M</li>`;
        }
        if(element.subtarifas[0].subtarifa_cent_minuto && element.subtarifas[0].subtarifa_est_llamada){
            options +=`<li>${element.subtarifas[0].subtarifa_cent_minuto} Cents/Min, ${element.subtarifas[0].subtarifa_est_llamada}/Cents Est.</li>`;
        }
        if(element.subtarifas[0].subtarifa_minutos_ilimitados){
            options +=`<li>Minutos Ilimitados</li>`;
        }
        if(element.subtarifas[0].subtarifa_precio_sms){
            options +=`<li>${element.subtarifas[0].subtarifa_cent_minuto} Cents/SMS</li>`;
        }
        if(element.subtarifas[0].subtarifa_velocidad_conexion_bajada && element.subtarifas[0].subtarifa_velocidad_conexion_subida){
            options +=`<li>${element.subtarifas[0].subtarifa_velocidad_conexion_bajada} Mb Up, ${element.subtarifas[0].subtarifa_velocidad_conexion_subida} Mb Down</li>`;
        }
        if(element.subtarifas[0].subtarifa_num_canales){
            options +=`<li>${element.subtarifas[0].subtarifa_num_canales} TV Cls</li>`;
        }
        try{
            if(index===0){
                document.querySelector("#tarifes").innerHTML = section(element, options);
            }else{
                document.querySelector("#tarifes").innerHTML += section(element, options);
            }
            
        }catch(e){
            console.log("error");
        };
    });
}

let section = (element, options) =>`
        <article class="main--position-quarter card card-1">
            <img class="logo" src="${element.logo}" />
            <h1 class="nombretarifa">${element.nombretarifa}</h1>
            <h2 class="precio">${element.precio} €</h2>
            <ul>${options}</ul>
        </article>`;

export {HomeController, SectionController};