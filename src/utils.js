let CACHE_TEMPLATES = new Map();
import {Settings} from './settings';

/** From Jake Archibald's Promises and Back: 
 *  http://www.html5rocks.com/en/tutorials/es6/promises/#toc-promisifying-xmlhttprequest
*/


function get(url, method = "GET", data) {
  if(method==="POST"){
    fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      } 
    })
      .then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(response => console.log('Success:', response));
  }else{
    /** Return a new promise. */
    return new Promise(function(resolve, reject) {
      if (CACHE_TEMPLATES.has(url)) {
        resolve(CACHE_TEMPLATES.get(url));
      }else{
        /** Do the usual XHR stuff */
        var req = new XMLHttpRequest();
        req.open('GET', url=Settings.baseURL+url);
    
        req.onload = function() {
          /** This is called even on 404 etc
          *   so check the status 
          */
          if (req.status == 200) {
            /** Resolve the promise with the response text */
            CACHE_TEMPLATES.set(url,req.response);
            resolve(req.response);
          }
          else {
            /** Otherwise reject with the status text
            *   which will hopefully be a meaningful error 
            */
            reject(Error(req.statusText));
          }
        };
    
        /** Handle network errors */
        req.onerror = function() {
          reject(Error("Network Error"));
        };
    
        /** Make the request */
        req.send();
      }
      
    });
  }
}

function setCookie(cname, cvalue, exdays) {
  if (cvalue && cvalue !== '') {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  }
}
function getCookie(cname) {
  let re = new RegExp('[; ]'+cname+'=([^\\s;]*)');  
  return (cname && (' '+document.cookie).match(re))? unescape((' '+document.cookie).match(re)[1]) : null;
}

function deleteCookie(name) {
  document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}


export {get, setCookie, getCookie, deleteCookie};